#include <iostream>

#include "board_io.h"
#include "delay.h"
#include "gpio.h"

using namespace std;

typedef struct {
  float f1; // 4 bytes
  char c1;  // 1 byte
  float f2;
  char c2;
} /*__attribute__((packed))*/ my_s;

int main(void) {

    // Made a struct named "s", and added values just cause.
    my_s s;
    s.f1 = 0xFF;
    s.c1 = 'A';
    s.f2 = 0xFF;
    s.c2 = 'B';
    
    printf("Size : %d bytes\n"
           "floats 0x%p 0x%p\n"
           "chars  0x%p 0x%p\n",
           sizeof(s), &s.f1, &s.f2, &s.c1, &s.c2);


  const gpio_s led = board_io__get_led0();

  while (true) {
    gpio__toggle(led);
    delay__ms(500);
  }

  return 1; // main() shall never return
}
